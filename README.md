## Ejercicio sesión 9

1 - Dado un JSON que define los campos input de un formulario, crear un componente Form que tenga dichos campos.

```
[
  {
    "name": "name",
    "label": "Nombre del dispositivo",
    "type": "text",
    "required": true
  },
  {
    "name": "brand",
    "label": "Marca del dispositivo",
    "type": "text",
    "required": true
  },
  {
    "name": "descripcion",
    "label": "Descripción",
    "type": "text"
  },
  {
    "name": "price",
    "label": "Precio",
    "type": "number"
  },
  {
    "name": "inches",
    "label": "Pulgadas",
    "type": "number"
  },
  {
    "name": "camera",
    "label": "MP Cámara",
    "type": "number"
  }
]
```

2 - Cuando componente Form haga submit, crearemos un objeto que representa un móvil con el siguiente formato (onSubmit):

```
{
  "name": "",
  "brand": "",
  "descripcion": "",
  "price": "",
  "inches": "",
  "camera": ""
}
```

3 - Enviar el móvil desde el componente Form al estado que tengamos en App (ya sea por Context o con una función normal).
4 - Crear un componente Catalog que pinte nuestros móviles en una ruta (React Router) `/catalog`. Los móviles que lleguen a esta ruta estarán en el estado de App, pero vendrán a través de Context.
5 - Escuchar cambios en el estado de App y mostrar un toast cada vez que se añada un nuevo móvil al catálogo.
6 - Mostrar un toast cuando se borre un móvil del catálogo. (Botón de borrar en cada móvil).