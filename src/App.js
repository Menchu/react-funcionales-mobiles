import React, { useState } from "react";
import { MobilesContext, DeleteContext } from "./context";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Form from "./components/Form";
import List from "./components/List";
import Navbar from "./components/Navbar";
import mobilesJson from "./info.json";
import "./App.scss";
import "react-toastify/dist/ReactToastify.css";
import { toast } from "react-toastify";
import { v4 as uuid } from "uuid";

const App = () => {
  const [mobiles, setMobiles] = useState(mobilesJson); //valor inicial(mobilesJson)

  const addMobile = (values) => {
    const newMobile = {
      id: uuid(),
      name: values.name,
      imageUrl:values.imageUrl,
      brand: values.brand,
      description: values.description,
      price: values.price,
      pixeles: values.pixeles,
      camera: values.camera,
    };

    setMobiles([...mobiles, newMobile]);
    toast.success("🎉 Se ha creado tu nuevo movil!📲 🎉 "); //opción ganadora!!!
  };

  // useEffect(() => {
  //   toast.success("🎉 Se ha creado tu nuevo movil!📲 🎉 ");
  // }, [mobiles]); segunda opcion para sacar el toast, pero le faltaria condicion de que no salte el primer renderizado

  const mobileToDelete = (id) => {
    setMobiles(
      mobiles.filter((mob) => {
        if (mob.id !== id) {
          return mob;
        }
      })
    );
  };

  return (
    <BrowserRouter>
      <div className="App">
        <MobilesContext.Provider value={[mobiles, addMobile]}>
          <DeleteContext.Provider value={[, mobileToDelete]}>
            <Navbar />
            <Switch>
              <Route exact path="/" component={Form} />

              <Route exact path="/catalog" component={List} />
            </Switch>
          </DeleteContext.Provider>
        </MobilesContext.Provider>
      </div>
    </BrowserRouter>
  );
};

export default App;
