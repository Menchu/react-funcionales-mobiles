import React from "react";
import "../styles/form.scss";
import { useState, useContext } from "react";
import { MobilesContext } from "../context";
import { ToastContainer } from "react-toastify";

const Form = () => {
  const [_, addMobile] = useContext(MobilesContext);
  const [formValues, setFormValues] = useState({
    name: "",
    imageUrl:"",
    brand: "",
    description: "",
    price: "",
    pixeles: "",
    camera: "",
  });
  const [error, setError] = useState(null);

  const handleSubmit = (ev) => {
    ev.preventDefault();

    const valuesToForm = formValues;
    const objValues = Object.values(valuesToForm);
    const validValues = objValues.filter((value) => Boolean(value));
    const isValid = objValues.length === validValues.length;

    if (isValid) {
      addMobile(formValues);
      // console.log(formValues);
      // toast.success('🎉 Se ha creado tu nuevo movil!📲 🎉 ');///opción 1: a lo mejor aquí habría que usar un useEfect??aunque así funciona!
      setFormValues(formValues);
    } else {
      setError("Todos los campos son requeridos");
    }
  };

  function handleChangeInput(ev) {
    const name = ev.target.name;
    const value = ev.target.value;

    setFormValues({
      ...formValues,
      [name]: value,
    });
  }

  return (
    <section className="section">
      <form className="MobileForm" onSubmit={handleSubmit}>
        <div className="MobileForm__containerForm">
          <h3>Selecciona tu Movil</h3>
          <label htmlFor="Nombre del dispositivo">
            <span>Nombre del dispositivo</span>
            <input
              className="name"
              type="text"
              name="name"
              value={formValues.name}
              onChange={handleChangeInput}
              required
            />
          </label>
          <label htmlFor="imageUrl">
          <span>Imagen</span>
          <input
            type="text"
            name="imageUrl"
            value={formValues.imageUrl}
            required
            onChange={handleChangeInput}
          />
        </label>
          <label htmlFor="brand">
            <span>Marca del dispositivo</span>
            <input
              type="text"
              name="brand"
              value={formValues.brand}
              onChange={handleChangeInput}
              required
            />
          </label>
          <label htmlFor="description">
            <span>Descripción</span>
            <input
              type="text"
              name="description"
              value={formValues.description}
              onChange={handleChangeInput}
              required
            />
          </label>

          <label htmlFor="price">
            <span>Precio</span>
            <select
              id="price"
              name="price"
              value={formValues.price}
              onChange={handleChangeInput}
            >
              <option value="">Seleciona una opción</option>
              <option value="Low">100 &#8364;</option>
              <option value="Medium">200 &#8364;</option>
              <option value="Hight">300 &#8364;</option>
            </select>
          </label>

          <label htmlFor="pixeles">
            <span>Pulgadas</span>
            <input
              type="number"
              name="pixeles"
              value={formValues.pixeles}
              onChange={handleChangeInput}
              required
            />
          </label>

          <label htmlFor="MP Cámara">
            <span>Cámara</span>
            <input
              type="string"
              name="camera"
              value={formValues.camera}
              onChange={handleChangeInput}
              required
            />
          </label>
        </div>
        <div className="MobileForm__buttons">
          <button>Guardar mi movil</button>
        </div>
        <ToastContainer position="bottom-right" />
      </form>
    </section>
  );
};

export default Form;
