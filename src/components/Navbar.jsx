import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome, faMobileAlt } from "@fortawesome/free-solid-svg-icons";
import "../styles/navbar.scss";
import header from "../header.png";

function Navbar() {
  return (
    <div className="Navbar">
      <div className="Navbar__img">
        <img className="Img" src={header} alt="imagen moviles"></img>
      </div>
      <div className="Navbar__links">
        <div className="Navbar__links--left">
          <Link to="/">
            <FontAwesomeIcon className="homeLink" icon={faHome} />
          </Link>
        </div>
        <div className="Navbar__links--right">
          <Link to="/catalog">
            {" "}
            Catálogo
            <FontAwesomeIcon className="homeLink" icon={faMobileAlt} />
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
