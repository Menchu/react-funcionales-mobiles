import MobileItem from "./MobileItem";
import "../styles/list.scss";

import React from "react";

const List = () => {
  //    console.log(mobiles)

  return (
    <div className="Catalog">
      <h2>Cátalogo de Moviles</h2>

      <MobileItem />
    </div>
  );
};

export default List;
