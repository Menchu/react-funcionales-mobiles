import React, { useContext } from "react";
import { MobilesContext, DeleteContext } from "../context";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import "../styles/mobileItem.scss";
const MobileItem = (id) => {
  const [mobiles] = useContext(MobilesContext);

  const [, mobileToDelete] = useContext(DeleteContext);
  // console.log(mobiles)

  return (
    <ul className="cardList">
      {mobiles.map((mob) => (
        <li className="cardList__card" key={mob.id + mob.name}>
          <h3>{mob.name}</h3>
          <img src={mob.imageUrl} alt={mob.name} width="120" />
          <p>{mob.brand}</p>
          <p>{mob.descripcion}</p>
          <p>{mob.price}</p>
          <p>{mob.pixeles}</p>
          <p>{mob.camera}</p>
          <button onClick={() => mobileToDelete(mob.id)}>
            <FontAwesomeIcon className="homeLink" icon={faTrash} />
          </button>
        </li>
      ))}
    </ul>
  );
};
export default MobileItem;
